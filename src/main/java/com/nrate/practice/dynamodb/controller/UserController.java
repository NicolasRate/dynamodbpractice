package com.nrate.practice.dynamodb.controller;

import com.nrate.practice.dynamodb.entities.User;
import com.nrate.practice.dynamodb.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/query/{userId}")
    public List<User> getUsers(@PathVariable String userId) {
        log.info("getUsers");
        log.info("UserId: {}", userId);

        List<User> users = userService.getUsers(userId);

        log.info("Returned users {\n{}\n}", users.stream().map(User::toString).collect(Collectors.joining("\n")));

        return users;
    }

    @PostMapping("/user")
    public User getUser(@RequestBody User user) {
        log.info("getUser");
        log.info("User: {}", user);

        if (user.getUserId() == null || user.getBirthDate() == null) {
            throw new RuntimeException("UserId or BirthDate is null");
        }

        User resultedUser = userService.getUser(user);

        log.info("Resulted user: {}", resultedUser);

        return resultedUser;
    }

    @PostMapping
    public void createUser(@RequestBody User user) {
        log.info("createUser");
        log.info("User: {}", user);

        if (user.getUserId() == null || user.getBirthDate() == null) {
            throw new RuntimeException("UserId or BirthDate is null");
        }

        userService.createUser(user);

        log.info("user is created");
    }

    @PutMapping
    public void updateUser(@RequestBody User user) {
        log.info("updateUser");
        log.info("User: {}", user);

        if (user.getUserId() == null || user.getBirthDate() == null) {
            throw new RuntimeException("UserId or BirthDate is null");
        }

        userService.updateUser(user);

        log.info("user is updated");
    }

    @DeleteMapping
    public void deleteUser(@RequestBody User user) {
        log.info("delete");
        log.info("User: {}", user);

        if (user.getUserId() == null || user.getBirthDate() == null) {
            throw new RuntimeException("UserId or BirthDate is null");
        }

        userService.deleteUser(user);

        log.info("user is deleted");
    }
}
