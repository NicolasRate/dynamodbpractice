package com.nrate.practice.dynamodb.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.nrate.practice.dynamodb.entities.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final DynamoDBMapper dynamoDBMapper;

    public List<User> getUsers(String userId) {
        DynamoDBQueryExpression<User> queryExpression = new DynamoDBQueryExpression<User>()
                .withHashKeyValues(User.builder().userId(userId).build())
                .withLimit(10);

        return dynamoDBMapper.query(User.class, queryExpression);
    }

    public User getUser(User user) {
        return dynamoDBMapper.load(user);
    }

    public void createUser(User user) {
        dynamoDBMapper.save(user);
    }

    public void updateUser(User user) {
        User loadedUser = getUser(user);

        loadedUser.setFirstName(user.getFirstName());
        loadedUser.setLastName(user.getLastName());
        loadedUser.setAge(user.getAge());
        loadedUser.setEmail(user.getEmail());

        dynamoDBMapper.save(user);
    }

    public void deleteUser(User user) {
        User loadedUser = getUser(user);
        dynamoDBMapper.delete(loadedUser);
    }

}
