package com.nrate.practice.dynamodb.entities;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import lombok.*;

@ToString
@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
@DynamoDBTable(tableName = "practice-table")
public class User {

    @DynamoDBHashKey(attributeName = "user_id")
    private String userId;

    @DynamoDBRangeKey(attributeName = "birth_date")
    private String birthDate;

    @DynamoDBAttribute(attributeName = "firstName")
    private String firstName;

    @DynamoDBAttribute(attributeName = "lastName")
    private String lastName;

    @DynamoDBAttribute(attributeName = "age")
    @DynamoDBIndexRangeKey(attributeName = "age", localSecondaryIndexName = "age-index")
    private Integer age;

    @DynamoDBAttribute(attributeName = "email")
    private String email;

}
